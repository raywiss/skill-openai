import openai
from opsdroid.skill import Skill
from opsdroid.constraints import constrain_rooms
from opsdroid.matchers import match_regex
from opsdroid.events import Message
import logging

# Replace YOUR_API_KEY with your actual API key
openai.api_key = "YOUR_API_KEY"


class ChatGPT(Skill):

    def __init__(self, opsdroid, config):
        super(ChatGPT, self).__init__(opsdroid, config)
        # do some setup stuff here
        logging.debug("Loaded feed module")
        self.api_key=None

    @match_regex(r"^ChatGPT help$")
    @constrain_rooms(['chatgptmaster'])
    async def chatgpt_help(self, message):
        
        # Send the generated text back to the user
        await message.respond("TBD")

    @match_regex(r"^cgpt: (?P<prompt>.*)$|^CGPT: (?P<prompt>.*)$|^chatgpt: (?P<prompt>.*)$|^ChatGPT: (?P<prompt>.*)$")
    @constrain_rooms(['chatgptmaster','chatgpt1','chatgpt2','chatgpt3'])
    async def generate_text(self, message):
        if not self.api_key:
            self.api_key = await self.opsdroid.memory.get("openai")
            if self.api_key == None:
                await message.respond("You have not set the api key.")
                return
        # Use the GPT-3 API to generate text
        openai.api_key = self.api_key
        prompt = message.regex.group('prompt')
        engine = self.config.get("engine")
        max_tokens = self.config.get("max_tokens")
        result = openai.Completion.create(engine=engine, 
            prompt=prompt, max_tokens=max_tokens)
        #result={}
        #result['choices']=[{'text': 'tbd'}]
        text = result['choices'][0]['text']
        
        # Send the generated text back to the user
        await message.respond("ChatGPT: "+str(text))

    @match_regex(r"^ChatGPT token: (?P<token>.*)$")
    @constrain_rooms(['chatgptmaster'])
    async def set_token(self, message):
        # Use the GPT-3 API to generate text
        token = message.regex.group('token')
        await self.opsdroid.memory.put("openai", token)
        await message.respond("Token set.")

